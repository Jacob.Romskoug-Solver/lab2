package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int capasity = 20;
    ArrayList <FridgeItem> itemInFridge = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return itemInFridge.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return capasity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (itemInFridge.size() >= totalSize()){
            return false;
        }
        return itemInFridge.add(item);
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (!itemInFridge.contains(item)){
            throw new NoSuchElementException("Item is not in fridge fatfuck!");
        }
        else{
            itemInFridge.remove(item);
        }
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        itemInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List <FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : itemInFridge){
            if (item.hasExpired()){
                expiredItems.add(item);
            }
        }
        for (FridgeItem expFood : expiredItems) {
            itemInFridge.remove(expFood);
        }
        return expiredItems;
    }
}
